package ua.alex1rap;

import java.util.Base64;

public class TextCoder {

    protected static String crypt(String text, String passHash, int num) {
        long lastTime = System.currentTimeMillis();
        int lastPoint = 0;
        int len = text.length();
        char[] encoded = new char[text.length()];
        for (int i = 0; i < len; i++) {
            char textChar = text.charAt(i);
            char passChar = passHash.charAt(i % 32);
            char code = (char) (passChar ^ num);
            encoded[i] = (char)(textChar ^ code);
            if (i - lastPoint > 1024 && System.currentTimeMillis() - lastTime >= 10) {
                lastTime = System.currentTimeMillis();
                lastPoint = i;
                System.out.print("\r Processed " + i + " bytes from " + len + " bytes");
            }
        }
        return new String(encoded);
    }

    public static String encode(String text, String pass) {
        String passHash = Hash.md5(pass);
        String result = crypt(text, passHash, pass.length());
        return Base64.getEncoder().encodeToString(result.getBytes());
    }

    public static String decode(String textCode, String pass) {
        byte[] textBytes = Base64.getDecoder().decode(textCode);
        String text = new String(textBytes);
        String passHash = Hash.md5(pass);

        String result = crypt(text, passHash, pass.length());

        return result;
    }
}
