package ua.alex1rap;

import java.io.*;
import java.util.Base64;
import java.util.Scanner;

public class FileCoder {

    public static final String APP_VERSION = "2.0.5";

    protected static String readFromTextFile(String name) throws IOException {
        FileReader fileReader = new FileReader(name);
        Scanner scanner = new Scanner(fileReader);
        String text = "";

        int i = 0;
        while (scanner.hasNextLine()) {
            String line = scanner.nextLine();
            text += (i++ > 0) ? "\n" + line : line;
        }
        fileReader.close();
        return text;
    }

    protected static byte[] readBytesFromFile(String name) throws IOException {

        File originalFile = new File(name);
        FileInputStream fileInputStreamReader = new FileInputStream(originalFile);
        byte[] bytes = new byte[(int)originalFile.length()];
        fileInputStreamReader.read(bytes);
        return Base64.getEncoder().encode(bytes);
    }

    protected static void writeToTextFile(String name, String text) throws IOException {
        FileWriter fileWriter = new FileWriter(name);
        fileWriter.write(text);
        fileWriter.close();
    }

    public static String encode(String name, String pass) throws IOException {
        return encode(name, pass, true);
    }

    public static String encode(String name, String pass, boolean encodeFileName) throws IOException {
        File file = new File(name);
        String filePath = file.getAbsolutePath();
        String fileName = file.getName();
        System.out.println("Reading from '" + filePath + "'...");

        //String text = Base64.getEncoder().encodeToString(readBytesFromFile(filePath));
        //String text = readFromTextFile(filePath);
        String text = new String(readBytesFromFile(filePath));

        String encodedFileName;
        String encodedText = TextCoder.encode(text, pass);

        if (encodeFileName) {
            encodedFileName = TextCoder.encode(fileName, pass);
        } else {
            encodedFileName = fileName + ".encoded";
        }
        File encodedFile = new File(encodedFileName);
        System.out.println();
        System.out.println("Writing to '" + encodedFile.getAbsolutePath() + "'...");
        writeToTextFile(encodedFile.getAbsolutePath(), encodedText);
        System.out.println("Done. Written " + encodedFile.length() + " bytes.");

        return encodedFile.getAbsolutePath();
    }

    public static String decode(String name, String pass) throws IOException {
        return decode(name, pass, true);
    }

    public static String decode(String name, String pass, boolean decodeFileName) throws IOException {
        File file = new File(name);
        String filePath = file.getAbsolutePath();
        String fileName = file.getName();
        String text = readFromTextFile(filePath);
        System.out.println("Reading from '" + filePath + "'...");

        String decodedFileName;
        //String decodedText = new String(Base64.getDecoder().decode(TextCoder.decode(text, pass)));
        //String decodedText = "";
        byte[] chars;
        try {
            chars = Base64.getDecoder().decode(TextCoder.decode(text, pass));
        } catch (IllegalArgumentException e) {
            chars = new byte[0];
            System.out.println();
            System.err.println(" Error: Unable to decrypt file. It may be damaged or password is invalid.");
            System.exit(-1);
        }

        if (decodeFileName) {
            decodedFileName = TextCoder.decode(fileName, pass);
        } else {
            decodedFileName = fileName + ".decoded";
        }
        File decodedFile = new File(decodedFileName);
        //writeToTextFile(decodedFile.getAbsolutePath(), decodedText);
        FileOutputStream fs = new FileOutputStream(decodedFile);
        System.out.println();
        System.out.println("Writing to '" + decodedFile.getAbsolutePath() + "'...");
        fs.write(chars);
        fs.close();
        System.out.println("Done. Written " + decodedFile.length() + " bytes.");
        return decodedFile.getAbsolutePath();
    }

    public static void main(String[] args) throws Exception {

        System.out.println("Coder by alex1rap v" + APP_VERSION);

        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter password for coder:");
        String pass = scanner.nextLine();
        System.out.println("\nEnter file name:");
        String name = scanner.nextLine();
        File file = new File(new File(name).getAbsolutePath());
        while (!file.exists()) {
            System.out.println("File '" + file.getAbsolutePath() + "' don't exists.");
            System.out.println("\nEnter file name:");
            name = scanner.nextLine();
            file = new File(new File(name).getAbsolutePath());
        }
        System.out.println("\nSelect one from next options:");
        System.out.println("1. Encode");
        System.out.println("2. Decode");
        String option = scanner.nextLine();
        while (!(option.equals("1") || option.equals("2"))) {
            System.out.println();
            System.out.println("'" + option + "' is not supported number. Enter 1 or 2:");
            System.out.println("1. Encode");
            System.out.println("2. Decode");
            option = scanner.nextLine();
        }
        boolean cryptFileName = true;
        String cryptedFileName = file.getAbsolutePath();

        if (option.equals("1")) {
            cryptedFileName = encode(file.getAbsolutePath(), pass, cryptFileName);
        }
        if (option.equals("2")) {
            cryptedFileName = decode(file.getAbsolutePath(), pass, cryptFileName);
        }

        System.out.println("New file saved at '" + cryptedFileName + "'.");
    }
}
